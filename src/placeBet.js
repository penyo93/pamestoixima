
const  placeBet = (oddsID, marketID,betSize,marketOdds) => {

    fetch("https://taxation.pamestoixima.gr/potential-payout-service/api/v1/potential-payouts", {
        "headers": {
            "accept": "*/*",
            "accept-language": "hu-HU,hu;q=0.9,en-US;q=0.8,en;q=0.7",
            "authorization": "Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJ1YVFWb18xTU5YUkF1bFFaTjhRX3pzYzlHVGljLUJXaUZuUTF1MW5TY1MwIn0.eyJleHAiOjE2ODg5NzI0NTAsImlhdCI6MTY4ODk3MjE1MCwiYXV0aF90aW1lIjoxNjg4OTcyMTQ5LCJqdGkiOiJmNTc4YmY2My0wYTk2LTQxNDgtOTQxYy1lYzAxMWI0OTAxMTgiLCJpc3MiOiJodHRwczovL2lhYW1zLnBhbWVzdG9peGltYS5nci9pYW0tc2VydmljZS9hdXRoL3JlYWxtcy9vcGVuZW5nYWdlIiwic3ViIjoiMS04MzMyMTg3NDkiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJ3ZWJhcHAtY2xpZW50Iiwibm9uY2UiOiIxNGU3NThjNi03N2I0LTQ2ODktYTQ1Ny01MWRmNzY1OTU2NGYiLCJzZXNzaW9uX3N0YXRlIjoiM2ZlOWZiNTEtODdmOS00MTJiLThmNjYtYjVkNzkzOWE3MWI0IiwiYWNyIjoiMSIsInNjb3BlIjoib3BlbmlkIG9pZGMgcHJvbW90aW9uIGNvbW1vbiBzcG9ydHNib29rIn0.LEz9bmh0XBuga2leWgMuHt5nm9oIJc5ePdvIcQWsdQU1tic5_zy5ST6vNptRrwmEkN5Sh8UVnooH9z713lmjcLUK8IFC87FBCacc9khXOmscY4HWe0NiKquCXOjr63RQfhoKDj40j2gFctl1UU6KPOicUcDGyXas1KgNvLqvDy4-AtMGLe47u8yiYWJWTgQJE52GqoEznwkpJ0tHFiaIM6QG1_R6h-tlHEy2uT2qBZu66yK7QO0WVEFqSW8eWKE0ojcznKzkDRwVLOOE6tutacABRQ_mr87CGZdfqLDhqAH260xk0_J0BRvpVeyHSliEFlbk9byIqPYgWZBdDpaKDw",
            "content-type": "application/json",
            "sec-ch-ua": "\"Not.A/Brand\";v=\"8\", \"Chromium\";v=\"114\", \"Google Chrome\";v=\"114\"",
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": "\"Windows\"",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-site",
            "x-accept-language": "en-GB",
            "x-ob-channel": "I",
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36",
            "Referer": "https://www.pamestoixima.gr/",
            "Referrer-Policy": "strict-origin-when-cross-origin"
        },
        "body": `{\"legs\":[{\"documentId\":\"LEG1\",\"outcomes\":[{\"outcomeRef\":\"${oddsID}\"}]}],\"outcomes\":[{\"id\":\"${oddsID}\",\"marketRef\":\"${marketID}\"}],\"bets\":[{\"documentId\":\"BET1\",\"betTypeRef\":\"SGL\",\"lines\":1,\"legRefs\":[\"LEG1\"],\"maximumPayout\":\"9999999\",\"minimumStake\":\"0.1\",\"maximumStake\":\"0.92\",\"potentialPayout\":[{\"type\":\"MULTIPLIER\",\"winPlaceOverrideRef\":\"WIN\",\"value\":\"${marketOdds}\"}],\"stake\":\"${betSize}\"}]}`,
        "method": "POST"
    }).then(response => response.json()).then(data => console.log(data)).catch(error => console.log(error));
}

//{"legs":[{"documentId":"LEG1","outcomes":[{"outcomeRef":"53003516"}]}],"outcomes":[{"id":"53003516","marketRef":"19891613"}],"bets":[{"documentId":"BET1","betTypeRef":"SGL","lines":1,"legRefs":["LEG1"],"maximumPayout":"9999999","minimumStake":"0.1","maximumStake":"0.92","potentialPayout":[{"type":"MULTIPLIER","winPlaceOverrideRef":"WIN","value":"4.25"}],"stake":"0.10"}]}