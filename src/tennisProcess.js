let matches = [{
    id: "",
    sport: "",
    leagueName: "",
    teams: "",
    class: "",
    matchTime: "",
    markets: [{
        id: "",
        status: "",
        marketName: "",
        odds: [{
            id:"",
            teamName: "",
            odds: ""
        }]
    }],
}]

const tennisProcess = (data) => {

    let jsonData = JSON.parse(data)
    jsonData.data.events.forEach(it => {
        let marketArray = []
        it.markets.map(market => {
            marketArray.push({
                id: market.id,
                status: market.status,
                marketName: market.groupCode,
                odds:[
                    {
                        id: market.outcomes[0].id,
                        teamName: market.outcomes[0].name,
                        odds: market.outcomes[0].prices[0].decimal
                    },
                    {
                        id: market.outcomes[1].id,
                        teamName: market.outcomes[1].name,
                        odds: market.outcomes[1].prices[0].decimal
                    }
                ]
            })
        })
        matches.push({
            id: it.id,
            sport: it.category.name,
            leagueName: it.class.name,
            teams: it.name,
            matchTime: it.startTime.split('T')[0] + " date " + it.startTime.split('T')[1].replace('Z', ''),
            markets: marketArray
        })

    })

    return matches;
}


export {tennisProcess}