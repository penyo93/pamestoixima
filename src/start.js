import puppeteer from 'puppeteer'
import {tennisProcess} from "./tennisProcess.js";
import {basketBallProcess} from "./basketBallProcess.js"



let sportName = "TENNIS";

(async () => {
    await puppeteer
        .launch({
            args: [
                '--disable-web-security',
                '--disable-features=IsolateOrigins',
                '--disable-site-isolation-trials'
            ],
            headless: false,
        })
        .then(async (browser) => {
            const basketBallPage = await browser.newPage();

            const tennisPage = await browser.newPage();

            await basketBallPage.goto("https://capi.pamestoixima.gr/content-service/api/v1/q/eventsListBySport?drilldownTagIds=5&groupCodes=HANDICAP_2_WAY,TOTAL_POINTS_OVER/UNDER,MONEY_LINE&sortsIncluded=MTCH&liveNow=true&orderEventsBy=displayOrder&excludeEventsWithNoMarkets=false", {
                waitUntil: "load",
                timeout: 0,
            });
            await tennisPage.goto("https://capi.pamestoixima.gr/content-service/api/v1/q/eventsListBySport?drilldownTagIds=12&groupCodes=MATCH_WINNER,TOTAL_GAMES_OVER/UNDER,TOTAL_SETS_OVER/UNDER,GAME_HANDICAP&sortsIncluded=MTCH&liveNow=true&orderEventsBy=displayOrder&excludeEventsWithNoMarkets=fals", {
                waitUntil: "load",
                timeout: 0,
            });
            setInterval(async () => {
                if (sportName === "TENNIS") {
                    await tennisPage.reload();
                    await tennisPage.setDefaultNavigationTimeout(0);
                    await tennisPage.waitForNavigation();
                    const tennisExtractedText = await tennisPage.$eval('*', (el) => el.innerText);
                    tennisProcess(tennisExtractedText)
                }
                else {
                    await basketBallPage.reload();
                    await basketBallPage.setDefaultNavigationTimeout(0);
                    await basketBallPage.waitForNavigation();
                    const basketBallExtractedText = await basketBallPage.$eval("*",(el) => el.innerText);
                    basketBallProcess(basketBallExtractedText)
                }
            },1000)
        });
})()
